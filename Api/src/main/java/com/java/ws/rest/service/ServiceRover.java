package com.java.ws.rest.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.java.ws.rest.rover.Positions;

@Path("/rover")
public class ServiceRover {

	@POST
	@Path("/move")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Positions posicionFinal(Positions rv) {

		// Obtener coordenadas x e y
		
		String tempPos = "";
		
		int x = 0;
		String originalPositionX;
		Boolean isNegX = (rv.getOriginalPosition().substring(0, 1)).equalsIgnoreCase("-");
		
		if (isNegX){
			String originalPositionXNeg = rv.getOriginalPosition().substring(0, 2);
			 x = Integer.parseInt(originalPositionXNeg);
			 tempPos = rv.getOriginalPosition().substring(2, rv.getOriginalPosition().length());
		} else if(!isNegX) {
			originalPositionX = rv.getOriginalPosition().substring(0, 1);
			 x = Integer.parseInt(originalPositionX);
			 tempPos = rv.getOriginalPosition().substring(1, rv.getOriginalPosition().length());
		}
		
		Boolean isNegY = (tempPos.substring(0, 1)).equalsIgnoreCase("-");
		String originalPositionYNeg;
		String originalPositionY;
		int y = 0;
		
		if (isNegY){
			originalPositionYNeg = tempPos.substring(0, 2);
			y = Integer.parseInt(originalPositionYNeg);
			tempPos = tempPos.substring(2, tempPos.length());
		} else if(!isNegY) {
			originalPositionY = tempPos.substring(0, 1);
			y = Integer.parseInt(originalPositionY);
			tempPos = tempPos.substring(1, tempPos.length());
		}
		
		// Obtener la orientacion N, S, E, W
		String orientation = tempPos.substring(0, 1);
		
		// Obtener las instrucciones de movimientos F, B, R, L
		String instructions = tempPos.substring(1, tempPos.length());
		
		char[] charArr = instructions.toCharArray();

		String finalOrientation = "";

		char o = orientation.charAt(0);

		// Hay un caso para cada orientacion (N, S, E, W), luego se recorre el array de chars
		// para cada movimiento realizado y finalmente se deja la orientacion del ultimo
		// caracter como orientacion final segun los puntos cardinales
		switch (o) {
		case 'N':

			for (char inst : charArr) {
				if (inst == 'F') {
					y = y + 1;
					finalOrientation = "North";
				} else if (inst == 'B') {
					y = y - 1;
					finalOrientation = "South";
				} else if (inst == 'R') {
					x = x + 1;
					finalOrientation = "East";
				} else if (inst == 'L') {
					x =  x - 1;
					finalOrientation = "West";
				}
			}
			break;
		case 'S':
			for (char inst : charArr) {
				if (inst == 'F') {
					y -= 1;
					finalOrientation = "South";
				} else if (inst == 'B') {
					y += 1;
					finalOrientation = "North";
				} else if (inst == 'R') {
					x -= 1;
					finalOrientation = "West";
				} else if (inst == 'L') {
					x += 1;
					finalOrientation = "East";
				}
			}
			break;
		case 'E':
			for (char inst : charArr) {
				if (inst == 'F') {
					x += 1;
					finalOrientation = "East";
				} else if (inst == 'B') {
					x -= 1;
					finalOrientation = "West";
				} else if (inst == 'R') {
					y -= 1;
					finalOrientation = "South";
				} else if (inst == 'L') {
					y += 1;
					finalOrientation = "North";
				}
			}
			break;
		case 'W':
			for (char inst : charArr) {
				if (inst == 'F') {
					x -= 1;
					finalOrientation = "West";
				} else if (inst == 'B') {
					x += 1;
					finalOrientation = "East";
				} else if (inst == 'R') {
					y += 1;
					finalOrientation = "North";
				} else if (inst == 'L') {
					y -= 1;
					finalOrientation = "South";
				}
			}
			break;
		}
		
		// Setear la posicion final del Rover
		rv.setFinalPosition("Coord.: (" + x + "," + y + "), Orientation: " + finalOrientation);
		return rv;
	}
}
