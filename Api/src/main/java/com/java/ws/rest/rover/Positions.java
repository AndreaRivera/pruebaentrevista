package com.java.ws.rest.rover;

public class Positions {
	private String originalPosition;
	private String finalPosition;

	public String getFinalPosition() {
		return finalPosition;
	}

	public void setFinalPosition(String finalPosition) {
		this.finalPosition = finalPosition;
	}

	public String getOriginalPosition() {
		return originalPosition;
	}

	public void setOriginalPosition(String originalPosition) {
		this.originalPosition = originalPosition;
	}
}
